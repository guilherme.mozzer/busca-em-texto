﻿namespace BuscaTexto
{
    partial class pesquisa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(pesquisa));
            this.txtLocalizar = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lblLocalizar = new System.Windows.Forms.Label();
            this.txtSubstituir = new System.Windows.Forms.TextBox();
            this.bntOK = new System.Windows.Forms.Button();
            this.bntCheckSubst = new System.Windows.Forms.CheckBox();
            this.bntCaseSensitive = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtLocalizar
            // 
            this.txtLocalizar.Location = new System.Drawing.Point(15, 25);
            this.txtLocalizar.Name = "txtLocalizar";
            this.txtLocalizar.Size = new System.Drawing.Size(233, 20);
            this.txtLocalizar.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // lblLocalizar
            // 
            this.lblLocalizar.AutoSize = true;
            this.lblLocalizar.Location = new System.Drawing.Point(12, 9);
            this.lblLocalizar.Name = "lblLocalizar";
            this.lblLocalizar.Size = new System.Drawing.Size(52, 13);
            this.lblLocalizar.TabIndex = 2;
            this.lblLocalizar.Text = "Localizar:";
            // 
            // txtSubstituir
            // 
            this.txtSubstituir.Location = new System.Drawing.Point(15, 74);
            this.txtSubstituir.Name = "txtSubstituir";
            this.txtSubstituir.Size = new System.Drawing.Size(233, 20);
            this.txtSubstituir.TabIndex = 3;
            this.txtSubstituir.Visible = false;
            // 
            // bntOK
            // 
            this.bntOK.BackColor = System.Drawing.Color.LightGreen;
            this.bntOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntOK.Location = new System.Drawing.Point(142, 114);
            this.bntOK.Name = "bntOK";
            this.bntOK.Size = new System.Drawing.Size(106, 30);
            this.bntOK.TabIndex = 5;
            this.bntOK.Text = "OK";
            this.bntOK.UseVisualStyleBackColor = false;
            this.bntOK.Click += new System.EventHandler(this.bntOK_Click);
            // 
            // bntCheckSubst
            // 
            this.bntCheckSubst.AutoSize = true;
            this.bntCheckSubst.Location = new System.Drawing.Point(15, 51);
            this.bntCheckSubst.Name = "bntCheckSubst";
            this.bntCheckSubst.Size = new System.Drawing.Size(71, 17);
            this.bntCheckSubst.TabIndex = 6;
            this.bntCheckSubst.Text = "Subistituir";
            this.bntCheckSubst.UseVisualStyleBackColor = true;
            this.bntCheckSubst.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // bntCaseSensitive
            // 
            this.bntCaseSensitive.AutoSize = true;
            this.bntCaseSensitive.Location = new System.Drawing.Point(15, 127);
            this.bntCaseSensitive.Name = "bntCaseSensitive";
            this.bntCaseSensitive.Size = new System.Drawing.Size(96, 17);
            this.bntCaseSensitive.TabIndex = 7;
            this.bntCaseSensitive.Text = "Case-Sensitive";
            this.bntCaseSensitive.UseVisualStyleBackColor = true;
            // 
            // pesquisa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(260, 156);
            this.Controls.Add(this.bntCaseSensitive);
            this.Controls.Add(this.bntCheckSubst);
            this.Controls.Add(this.bntOK);
            this.Controls.Add(this.txtSubstituir);
            this.Controls.Add(this.lblLocalizar);
            this.Controls.Add(this.txtLocalizar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "pesquisa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pesquisa";
            this.Load += new System.EventHandler(this.pesquisa_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLocalizar;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label lblLocalizar;
        private System.Windows.Forms.TextBox txtSubstituir;
        private System.Windows.Forms.Button bntOK;
        private System.Windows.Forms.CheckBox bntCheckSubst;
        private System.Windows.Forms.CheckBox bntCaseSensitive;
    }
}