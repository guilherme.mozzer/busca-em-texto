﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuscaTexto
{
    public partial class menu : Form
    {
        public menu()
        {
            InitializeComponent();
        }
        List<int> aux = new List<int>();
        string padrao, substituto;
        int auxsub = 0, rbusca, auxrbusca;
        pesquisa pesq = new pesquisa();

        private void ColorirPalavra(int PosicaoPalavra, string palavra)
        {
            texto.SelectionStart = PosicaoPalavra; //inicia a seleção
            texto.SelectionLength = palavra.Length; //seleciona ate o padrao.length
            texto.SelectionBackColor = Color.LightGreen;// colore o fundo do texto
        }

        private void SubstituirPalavra(int PosicaoPalavra, string palavra, string pSubstituto)
        {
            texto.SelectionStart = PosicaoPalavra;// inicia a seleção
            texto.SelectionLength = palavra.Length; //seleciona ate o padrao.length
            texto.SelectionBackColor = Color.LightGreen; // colore o fundo do texto
            texto.SelectedText = pSubstituto; //substitui o padrao pela palavra desejada
        }

        private void CorFundoPadrao()
        {
            lblBuscando.Visible = false;
            lblPalavrabuscada.Visible = false;
            lblPalavrabuscada.Text = "";
            texto.SelectionStart = 0;
            texto.SelectionLength = texto.TextLength; 
            texto.SelectionBackColor = Color.Transparent;//volta a cor do texto
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            texto.Text = "";
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Trabalho feito por:\nGuilherme Mozzer\nLeandro Laureano", "Sobre", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Arquivo texto | * .txt|Rich Text | * .rtf";
            ofd.Title = "Selecione um Arquivo .txt ou .rtf";
            ofd.ShowDialog();
            if (string.IsNullOrEmpty(ofd.FileName) == false)
            {
                try
                {
                    using (System.IO.StreamReader reader = new System.IO.StreamReader(ofd.FileName, Encoding.GetEncoding(CultureInfo.GetCultureInfo("pt-BR").TextInfo.ANSICodePage)))
                    {
                        texto.Text = reader.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Não foi possivel abrir o arquivo. Erro {0}", ex.Message), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void forçaBrutaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CorFundoPadrao();
            pesq.ShowDialog();

            if (pesq.Clickok())
            {
                ForcaBruta fb = new ForcaBruta();
                padrao = pesq.inserirLocalizar(); 
                substituto = pesq.inserirSubstituir();
                lblBuscando.Visible = true;
                lblPalavrabuscada.Visible = true;
                lblPalavrabuscada.Text = padrao;
                if (pesq.vCaseSensitive())
                {
                    do
                    {
                        auxrbusca = rbusca;
                        rbusca = fb.forcaBruta(padrao, texto.Text.Substring(rbusca)); //chamo o metodo de busca cortando o texto do padrão ate texto.TextLength, inialmente o rbusca = 0;
                        if (aux.Count() == 0 && rbusca == -1)//se a lista tiver vazia e rbusca foz -1 que dizer que percorreu todo o texto e não achou o padrão
                        {
                            aux.Add(rbusca);//adiiono na lista
                        }
                        else
                        {
                            if (rbusca != -1)//se rbusca for diferente de -1e porque encontrou o padrão no texto
                            {
                                aux.Add(rbusca+ auxrbusca); // adiciona na lista o valor de rbusca + a quantidade de caracteres que ja foi pesquisada;
                                rbusca += (auxrbusca + padrao.Length); //calcula onde cortar o texto para procurar novamente
                            }
                        }
                    } while (rbusca != -1);
                    rbusca = 0;
                }
                else
                {
                    do
                    {
                        auxrbusca = rbusca;
                        rbusca = fb.forcaBruta(padrao.ToLower(), texto.Text.ToLower().Substring(rbusca)); //se não for case sensitive
                        if (aux.Count() == 0 && rbusca == -1)
                        {
                            aux.Add(rbusca);
                        }
                        else
                        {
                            if (rbusca != -1)
                            {
                                aux.Add(rbusca + auxrbusca);
                                rbusca += (auxrbusca + padrao.Length);
                            }
                        }
                    } while (rbusca != -1);
                    rbusca = 0;
                }
                lblPalavrabuscada.Text += "         Ocorrências: " + aux.Count();
                foreach (int item in aux) //percorro a lista de palavras que achou
                {
                    if (item == -1)
                    {
                        MessageBox.Show("Palavra não encontrada!", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        if (pesq.vSubstituicao()) //verifico se no form pesquisa a o Checkbox de substituir foi marcado
                        {
                            SubstituirPalavra((item + auxsub), padrao, substituto); //chama o metodo de substituição
                            auxsub += substituto.Length - padrao.Length;//faz o calculo para colorir a proxima palavra corretamente
                        }
                        else
                        {
                            ColorirPalavra(item, padrao); // chama o metodo de colorir o fundo
                        }
                    }
                }
                auxsub = 0;
                aux.Clear(); //limpo a lista
            }
        }

        private void rabinKarpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CorFundoPadrao();
            pesq.ShowDialog();

            if (pesq.Clickok())
            {
                RabinKarp rk = new RabinKarp();
                padrao = pesq.inserirLocalizar();
                substituto = pesq.inserirSubstituir();
                lblBuscando.Visible = true;
                lblPalavrabuscada.Visible = true;
                lblPalavrabuscada.Text = padrao;
                if (pesq.vCaseSensitive())
                {
                    do
                    {
                        auxrbusca = rbusca;
                        rbusca = rk.RKSearch(padrao, texto.Text.Substring(rbusca));
                        if (aux.Count() == 0 && rbusca == -1)
                        {
                            aux.Add(rbusca);
                        }
                        else
                        {
                            if (rbusca != -1)
                            {
                                aux.Add(rbusca + auxrbusca);
                                rbusca += (auxrbusca + padrao.Length);
                            }
                        }
                    } while (rbusca != -1);
                    rbusca = 0;
                }
                else
                {
                    do
                    {
                        auxrbusca = rbusca;
                        rbusca = rk.RKSearch(padrao.ToLower(), texto.Text.ToLower().Substring(rbusca));
                        if (aux.Count() == 0 && rbusca == -1)
                        {
                            aux.Add(rbusca);
                        }
                        else
                        {
                            if (rbusca != -1)
                            {
                                aux.Add(rbusca + auxrbusca);
                                rbusca += (auxrbusca + padrao.Length);
                            }
                        }
                    } while (rbusca != -1);
                    rbusca = 0;
                }
                lblPalavrabuscada.Text += "         Ocorrências: " + aux.Count();
                foreach (int item in aux)
                {
                    if (item == -1)
                    {
                        MessageBox.Show("Palavra não encontrada!", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        if (pesq.vSubstituicao())
                        {
                            SubstituirPalavra((item + auxsub), padrao, substituto);
                            auxsub += substituto.Length - padrao.Length;
                        }
                        else
                        {
                            ColorirPalavra(item, padrao);
                        }
                    }
                }
                auxsub = 0;
                aux.Clear();
            }
        }

        private void kMPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CorFundoPadrao();
            pesq.ShowDialog();

            if (pesq.Clickok())
            {
                KMP kmp = new KMP();
                padrao = pesq.inserirLocalizar();
                substituto = pesq.inserirSubstituir();
                lblBuscando.Visible = true;
                lblPalavrabuscada.Visible = true;
                lblPalavrabuscada.Text = padrao;
                if (pesq.vCaseSensitive())
                {
                    do
                    {
                        auxrbusca = rbusca;
                        rbusca = kmp.KMPSearch(padrao, texto.Text.Substring(rbusca));
                        if (aux.Count() == 0 && rbusca == -1)
                        {
                            aux.Add(rbusca);
                        }
                        else
                        {
                            if (rbusca != -1)
                            {
                                aux.Add(rbusca + auxrbusca);
                                rbusca += (auxrbusca + padrao.Length);
                            }
                        }
                    } while (rbusca != -1);
                    rbusca = 0;
                }
                else
                {
                    do
                    {
                        auxrbusca = rbusca;
                        rbusca = kmp.KMPSearch(padrao.ToLower(), texto.Text.ToLower().Substring(rbusca));
                        if (aux.Count() == 0 && rbusca == -1)
                        {
                            aux.Add(rbusca);
                        }
                        else
                        {
                            if (rbusca != -1)
                            {
                                aux.Add(rbusca + auxrbusca);
                                rbusca += (auxrbusca + padrao.Length);
                            }
                        }
                    } while (rbusca != -1);
                    rbusca = 0;
                }
                lblPalavrabuscada.Text += "         Ocorrências: " + aux.Count();
                foreach (int item in aux)
                {
                    if (item == -1)
                    {
                        MessageBox.Show("Palavra não encontrada!", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        if (pesq.vSubstituicao())
                        {
                            SubstituirPalavra((item + auxsub), padrao, substituto);
                            auxsub += substituto.Length - padrao.Length;
                        }
                        else
                        {
                            ColorirPalavra(item, padrao);
                        }
                    }
                }
                auxsub = 0;
                aux.Clear();
            }
        }

        private void boyerMooreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CorFundoPadrao();
            pesq.ShowDialog();

            if (pesq.Clickok())
            {
                BoyerMoore bm = new BoyerMoore();
                padrao = pesq.inserirLocalizar();
                substituto = pesq.inserirSubstituir();
                lblBuscando.Visible = true;
                lblPalavrabuscada.Visible = true;
                lblPalavrabuscada.Text = padrao;
                if (pesq.vCaseSensitive())
                {
                    do
                    {
                        auxrbusca = rbusca;
                        rbusca = bm.BMSearch(padrao, texto.Text.Substring(rbusca));
                        if (aux.Count() == 0 && rbusca == -1)
                        {
                            aux.Add(rbusca);
                        }
                        else
                        {
                            if (rbusca != -1)
                            {
                                aux.Add(rbusca + auxrbusca);
                                rbusca += (auxrbusca + padrao.Length);
                            }
                        }
                    } while (rbusca != -1);
                    rbusca = 0;
                }
                else
                {
                    do
                    {
                        auxrbusca = rbusca;
                        rbusca = bm.BMSearch(padrao.ToLower(), texto.Text.ToLower().Substring(rbusca));
                        if (aux.Count() == 0 && rbusca == -1)
                        {
                            aux.Add(rbusca);
                        }
                        else
                        {
                            if (rbusca != -1)
                            {
                                aux.Add(rbusca + auxrbusca);
                                rbusca += (auxrbusca + padrao.Length);
                            }
                        }
                    } while (rbusca != -1);
                    rbusca = 0;
                }
                lblPalavrabuscada.Text += "         Ocorrências: " + aux.Count();
                foreach (int item in aux)
                {
                    if (item == -1)
                    {
                        MessageBox.Show("Palavra não encontrada!", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        if (pesq.vSubstituicao())
                        {
                            SubstituirPalavra((item + auxsub), padrao, substituto);
                            auxsub += substituto.Length - padrao.Length;
                        }
                        else
                        {
                            ColorirPalavra(item, padrao);
                        }
                    }
                }
                auxsub = 0;
                aux.Clear();
            }
        }
    }
}
