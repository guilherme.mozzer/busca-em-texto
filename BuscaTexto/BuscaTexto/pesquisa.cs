﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuscaTexto
{
    public partial class pesquisa : Form
    {
        public pesquisa()
        {
            InitializeComponent();
        }
        private static pesquisa _instance;
        private int click;

        public static pesquisa Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new pesquisa();
                return _instance;
            }
        }

        string valorL, valorS;
        public string inserirLocalizar()
        {

            if (txtLocalizar.Text != "")
            {
                valorL = txtLocalizar.Text;
            }
            else
            {
                valorL = "";
            }
            return valorL;
        }

        public string inserirSubstituir()
        {
            if (txtSubstituir.Text != "")
            {
                valorS = txtSubstituir.Text;
            }
            else
            {
                valorS = "";
            }
            return valorS;
        }

        public bool vSubstituicao()
        {
            if (bntCheckSubst.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool vCaseSensitive()
        {
            if (bntCaseSensitive.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Clickok()
        {
            if (click == 1)
                return true;
            else
                return false;
        }

        public void fechar()
        {
            _instance = null;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (bntCheckSubst.Checked)
            {
                txtSubstituir.Visible = true;
            }
            else
            {
                txtSubstituir.Visible = false;
            }
        }

        private void pesquisa_Load(object sender, EventArgs e)
        {
            click = 0;
            txtLocalizar.Text = "";
            txtSubstituir.Text = "";
            bntCheckSubst.Checked = false;
            bntCaseSensitive.Checked = false;
            txtLocalizar.Focus();
        }

        private void bntOK_Click(object sender, EventArgs e)
        {
            if (inserirLocalizar() != "")
            {
                    click = 1;
                    fechar();
                    Close();
            }
            else
            {
                txtLocalizar.Focus();
                MessageBox.Show("Digite uma Palavra ou frase!\n\nError: Campo Localizar não pode ser vazio.", "Atenção!");
            }
        }
    }
}
